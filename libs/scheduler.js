'use strict';

const redis = require('./redis');
const { logger, constants } = require('../utils');
const { send } = require('../services/emailService');

// Realiza a busca de emails agendados no servidor REDIS. Os registros são agrupados em um subconjunto definido por REDIS_KEYWORD.
function scheduledEmailQueue() {
  redis.scanAll(`${constants.namespace}:*`, (err, res) => {
    if (err) return;

    res.forEach(id => {
      redis.get(id, (err, res) => {
        if (err || !res) return;

        // Converte o registro armazenado em string no REDIS para JSON
        const request = JSON.parse(res);
        const now = new Date();
        request.sendDate = new Date(request.sendDate);

        // Verifica se a data de agendamento do email é menor que a data atual.
        if (request.sendDate - now <= 0) {
          delete request.sendDate;
          // Tenta enviar o email agendado. Caso aconteça algum erro, um email de falha é disparado para o remetente por meio de emailFailure.
          send({ request }, err => {
            // De qualquer forma, remove o email da lista de agendados
            redis.del(id, err => err && logger.error(err));
            if (err) return logger.error(err), emailFailure(request, err);

            logger.log(`Scheduled email sent: ${id}`);
          });
        }
      });
    });
  });
}

// Realiza o envio de email de falha para o remetente
function emailFailure(email, err) {
  const request = {
    from: email.from,
    to: email.from,
    cc: null,
    mapping: { ...email, err: err.toString() },
    subject: 'Falha de envio',
    attachments: [{ filename: 'email.html', content: email.html }],
    template: 'email_failure'
  };

  send({ request }, err => err && logger.error(err));
}

exports.init = callback => {
  // Executa a rotina de verificação de emails agendados a x ms.
  setInterval(scheduledEmailQueue, parseInt(process.env.EMAIL_TIMEOUT));
  callback();
};
