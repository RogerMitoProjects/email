'use strict';

const fs = require('fs');
const grpc = require('grpc');
const { logger } = require('../utils');
const email = require('../services/emailService');
const template = require('../services/templateService');
const { proto, middleware } = require('lib-protos');

/**
 * @description Obter as credenciais de conexão gRPC
 * @param {Function} callback - chamada de retorno `(err, creds) => {};`
 */
const getCredentials = callback => {
  if (process.env.ROOT_CERTS && process.env.PRIVATE_KEY && process.env.CERT_CHAIN) {
    try {
      const certs = {
        rootCerts: fs.readFileSync(process.env.ROOT_CERTS),
        keyCertPairs: [
          {
            cert_chain: fs.readFileSync(process.env.CERT_CHAIN),
            private_key: fs.readFileSync(process.env.PRIVATE_KEY)
          }
        ]
      };
      callback(null, proto.secureCredentials(certs));
    } catch (err) {
      logger.error(err);
      callback(err);
    }
  } else {
    callback(null, grpc.ServerCredentials.createInsecure());
  }
};

/**
 * @description Abre a conexão com o protocolo GRPC
 * @param {Function} callback - chamada de retorno `err => {};`
 */
const openConnection = callback => {
  getCredentials((err, creds) => {
    if (err) return callback(err);

    const { messenger } = proto.loadPackage();
    const { Email, Template } = messenger;
    const server = new grpc.Server();

    server.addService(Email.service, middleware.add(email));
    server.addService(Template.service, middleware.add(template));
    server.bind(process.env.GRPC_URL, creds);
    server.start();
    callback();
  });
};

module.exports = {
  openConnection
};
