'use strict';

require('dotenv').config();

const fs = require('fs');
const path = require('path');
const app = require('../app');
const uuid = require('uuid/v4');
const { proto } = require('lib-protos');
const { expect, should } = require('chai');

const from = process.env.TDD_EMAIL_FROM;
const to = [process.env.TDD_EMAIL_TO];

const template = uuid();
const emailBody = {
  from,
  to,
  cc: [],
  mapping: JSON.stringify({
    title: 'You received an email tested by mocha and sent via GRPC',
    label: 'Intro:',
    content:
      'gRPC (gRPC Remote Procedure Calls[1]) is an open source remote procedure call (RPC) system initially developed at Google. It uses HTTP/2 for transport, Protocol Buffers as the interface description language, and provides features such as authentication, bidirectional streaming and flow control, blocking or nonblocking bindings, and cancellation and timeouts.'
  }),
  subject: 'GRPC Test',
  template,
  attachments: [
    {
      filename: 'mocha.txt',
      content: 'VGVzdCE=',
      encoding: 'base64',
      contentType: 'text/plain'
    }
  ]
};

describe('Module: email (gRPC)', function() {
  let email;
  before(done => {
    app.init(err => {
      should().not.exist(err);

      const credentials = proto.createSSl({
        rootCerts: fs.readFileSync(`${__dirname}/ssl/ca.crt`),
        privateKey: fs.readFileSync(`${__dirname}/ssl/client.key`),
        certChain: fs.readFileSync(`${__dirname}/ssl/client.crt`)
      });
      const { Email } = proto.loadPackage().messenger;
      email = new Email(process.env.GRPC_URL, credentials);

      fs.copyFile(
        path.join(__dirname, 'templates', 'template.html'),
        path.join(process.env.TEMPLATE_PATH, `${template}.html`),
        err => {
          should().not.exist(err);
          done();
        }
      );
    });
  });

  after(() => {
    fs.unlinkSync(path.join(process.env.TEMPLATE_PATH, `${template}.html`));
  });

  it('Method: ping', done => {
    email.ping({}, (err, res) => {
      should().not.exist(err);
      expect(res.ping).to.be.equal('pong');
      done();
    });
  });

  this.timeout(10000);
  it('Method: send(immediate dispatch)', done => {
    email.send(emailBody, (err, res) => {
      should().not.exist(err);
      expect(res.id).to.be.a('string');
      expect(res.id.length).to.be.equal(36);
      done();
    });
  });
  this.timeout(2000);

  let scheduledId;
  it('Method: send(scheduled dispatch)', done => {
    email.send({ ...emailBody, sendDate: 72 }, (err, res) => {
      should().not.exist(err);
      expect(res.id).to.be.a('string');
      expect(res.id.length).to.be.equal(36);
      scheduledId = res.id;
      done();
    });
  });

  it('Method: cancel', done => {
    email.cancel({ id: scheduledId }, (err, res) => {
      should().not.exist(err);
      expect(res.result).to.be.equal(true);
      done();
    });
  });
});
