'use strict';

require('dotenv').config();

const fs = require('fs');
const path = require('path');
const uuid = require('uuid/v4');
const { http } = require('lib-commons');
const { expect, should } = require('chai');

const from = process.env.TDD_EMAIL_FROM;
const to = [process.env.TDD_EMAIL_TO];

const template = uuid();
const url = `https://localhost:${process.env.HTTP_PORT}/email`;
const cert = fs.readFileSync(process.env.ROOT_CERTS);

const emailBody = {
  from,
  to,
  cc: [],
  mapping: {
    title: 'The Cost Of JavaScript',
    label: 'Intro',
    content:
      'As we build sites more heavily reliant on JavaScript, we sometimes pay for what we send down in ways that we can’t always easily see. In this post, I’ll cover why a little discipline can help if you’d like your site to load & be interactive quickly on mobile devices.'
  },
  subject: 'Mocha Test - Final Review',
  template,
  attachments: [
    {
      filename: 'final.txt',
      content: 'VGVzdCE=',
      encoding: 'base64',
      contentType: 'text/plain'
    }
  ]
};

describe('Module: email (HTTPS)', () => {
  before(done => {
    fs.copyFile(
      path.join(__dirname, 'templates', 'template.html'),
      path.join(process.env.TEMPLATE_PATH, `${template}.html`),
      err => {
        should().not.exist(err);
        done();
      }
    );
  });

  after(() => {
    fs.unlinkSync(path.join(process.env.TEMPLATE_PATH, `${template}.html`));
  });

  it('Method: ping', done => {
    http.request({ url: `${url}/ping`, method: 'GET', body: {}, cert }, (err, res) => {
      should().not.exist(err);

      expect(res.ping).to.be.equal('pong');
      done();
    });
  });

  // Teste para envio imediato de email
  it('Method: send (immediate dispatch)', done => {
    http.request({ url: `${url}/v1/send`, method: 'POST', body: emailBody, cert }, (err, res) => {
      should().not.exist(err);
      expect(res.id).to.be.a('string');
      expect(res.id.length).to.be.equal(36);
      done();
    });
  });

  // Teste para agendamento de email
  let scheduledId;
  it('Method: send (scheduled dispatch)', done => {
    http.request({ url: `${url}/v1/send`, method: 'POST', body: { ...emailBody, sendDate: 72 }, cert }, (err, res) => {
      should().not.exist(err);
      expect(res.id).to.be.a('string');
      expect(res.id.length).to.be.equal(36);
      scheduledId = res.id;
      done();
    });
  });

  // Teste para cancelamento de um email inválido
  it('Method: cancel', done => {
    http.request({ url: `${url}/v1/send`, method: 'DELETE', body: { id: scheduledId }, cert }, (err, res) => {
      should().not.exist(err);
      expect(res.result).to.be.equal(true);
      done();
    });
  });

  // Teste para identificação de conteúdo inválido (marcador) para substituição no template
  it('Method: send (absent marker)', done => {
    http.request(
      {
        url: `${url}/v1/send`,
        method: 'POST',
        body: {
          ...emailBody,
          mapping: {
            absentTitle: 'The Cost Of JavaScript',
            absentLabel: 'Intro',
            absentContent:
              'As we build sites more heavily reliant on JavaScript, we sometimes pay for what we send down in ways that we can’t always easily see. In this post, I’ll cover why a little discipline can help if you’d like your site to load & be interactive quickly on mobile devices.'
          }
        },
        cert
      },
      err => {
        should().exist(err);
        done();
      }
    );
  });
});
