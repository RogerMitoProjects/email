'use strict';

require('dotenv').config();

const fs = require('fs');
const { http } = require('lib-commons');
const { should, expect } = require('chai');

const url = `https://localhost:${process.env.HTTP_PORT}/email/v1`;
const cert = fs.readFileSync(process.env.ROOT_CERTS);

describe('Module: template (HTTPS)', () => {
  let templateId;
  it('Method: create', done => {
    const body = '<body><h1>Mocha Test</h1> <br> <p style="font-color: red;"> {{user}} <br> {{adress}}</p></body>';
    http.request({ url: `${url}/template`, method: 'POST', contentType: 'text/html', body, cert }, (err, res) => {
      should().not.exist(err);
      expect(res.id).to.be.a('string');
      expect(res.id.length).to.be.equal(36);
      templateId = res.id;
      done();
    });
  });

  it('Method: delete', done => {
    http.request({ url: `${url}/template`, method: 'DELETE', body: { id: templateId }, cert }, (err, res) => {
      should().not.exist(err);
      expect(res.result).to.be.equal(true);
      done();
    });
  });
});
