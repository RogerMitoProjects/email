'use strict';

require('dotenv').config();

const fs = require('fs');
const { proto } = require('lib-protos');
const { expect, should } = require('chai');

describe('Module: template (gRPC)', () => {
  let template;
  before(() => {
    const credentials = proto.createSSl({
      rootCerts: fs.readFileSync(`${__dirname}/ssl/ca.crt`),
      privateKey: fs.readFileSync(`${__dirname}/ssl/client.key`),
      certChain: fs.readFileSync(`${__dirname}/ssl/client.crt`)
    });
    const { Template } = proto.loadPackage().messenger;
    template = new Template(process.env.GRPC_URL, credentials);
  });

  it('Method: ping', done => {
    template.ping({}, (err, res) => {
      should().not.exist(err);
      expect(res.ping).to.be.equal('pong');
      done();
    });
  });

  let templateId;
  it('Method: create', done => {
    const content = '<body><h1>Mocha Test</h1> <br> <p style="font-color: red;"> {{user}} <br> {{adress}}</p></body>';
    template.create({ content }, (err, res) => {
      should().not.exist(err);
      expect(res.id).to.be.a('string');
      expect(res.id.length).to.be.equal(36);
      templateId = res.id;
      done();
    });
  });

  it('Method: delete', done => {
    template.delete({ id: templateId }, (err, res) => {
      should().not.exist(err);
      expect(res.result).to.be.equal(true);
      done();
    });
  });
});
