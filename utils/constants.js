'use strict';

exports.emailSettings = email => ({
  transporter: {
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT,
    secure: true,
    auth: {
      user: process.env.SMTP_USER,
      pass: process.env.SMTP_PASSWORD
    }
  },
  options: {
    from: process.env.SMTP_USER,
    to: email.to,
    subject: email.subject,
    html: email.html,
    attachments: email.attachments
  }
});

exports.namespace = 'email';
