# email
[![pipeline status](https://gitlab.com/mastermaq/Discovery/email/badges/master/pipeline.svg)](https://gitlab.com/mastermaq/Discovery/email/commits/master)

Microsserviço de envio de email.

# Documentação
* Métodos públicos gRPC:
  * `ping`
    * IN:
    * OUT:
      * id: `{String}` - Em caso de sucesso retorna 'pong'
  * `send`
    * IN:
      * from: `{String}` - Email do remetente
      * to: `{Array}` - Array de strings que contém a lista de destinatários. Não pode ser vazio.
      * cc: `{Array}` - Array de strings que contém a lista de destinatários em cópia. Não pode ser vazio.
      * mapping: `{Object}` - Objeto contendo as informações para mapeamento de template. A estrutura do objeto deverá ser: {nome_marcacao: conteudo_campo} em que, nome_marcacao deve seguir a mesma nomenclatura definida na marcação do documento HTML.
      * subject: `{String}` - Assunto do email.
      * template: `{String}` - Identificador do arquivo de template que será utilizado pelo email.
      * attachments: `{Array}` - Array de objetos contendo os anexos do email. São permitidos até 5 anexos com um tamanho máximo de 10 MB cada. A estrutura do objeto deverá ser: {filename: nome_arquito.extensao, content: string_base64, encoding: base64, contentType: MIME_type Ex.:(application/pdf)}. São permitidos apenas arquivos condificados em base64.
      * sendDate: `{Integer}` * INteiro que representa o período em horas que o email permanecerá agendado. São aceitos valores entre 1 e 72 representando 1 hr e 72 hrs respectivamente.
    * OUT:
      * id: `{uuid}` - Identificador no formato uuid v4 que é retornado ao cliente em caso de agendamento de email. Esse identificador é utilizado na operação de cancelamento de agendamento.
  * `cancel`
    * IN:
      * id: `{uuid}` - Identificador no formato uuid v4 representando um email que está na lista de agendados. Caso o email não seja localizado, um erro 500 será retornado.
    * OUT:
  * `create`
    * IN:
      * content: `{string (text/html)}` - Conteúdo que será gravado no arquivo de template. A formatação deverá seguir os padrões definidos pelo html5.
    * OUT:
      * id: `{string}` - Identificador uuid/4 que representa o nome do template criado.
  * `delete`
    * IN:
      - id: `{string}` - Identificador no formato uuid/4 que representa o template a ser removido.
    * OUT:
* Métodos públicos HTTP:
  * `/email/v1/ping`
    * IN:
    * OUT:
      * `{String}` - Em caso de sucesso retorna 'pong'
  * `/email/v1/send`
    * IN:
      * from: `{String}` - Email do remetente
      * to: `{Array}` - Array de strings que contém a lista de destinatários. Não pode ser vazio.
      * cc: `{Array}` - Array de strings que contém a lista de destinatários em cópia. Não pode ser vazio.
      * mapping: `{Object}` - Objeto contendo as informações para mapeamento de template. A estrutura do objeto deverá ser: {nome_marcacao: conteudo_campo} em que, nome_marcacao deve seguir a mesma nomenclatura definida na marcação do documento HTML.
      * subject: `{String}` - Assunto do email.
      * template: `{String}` - Identificador do arquivo de template que será utilizado pelo email.
      * attachments: `{Array}` - Array de objetos contendo os anexos do email. São permitidos até 5 anexos com um tamanho máximo de 10 MB cada. A estrutura do objeto deverá ser: {filename: nome_arquito.extensao, content: string_base64, encoding: base64, contentType: MIME_type Ex.:(application/pdf)}. São permitidos apenas arquivos condificados em base64.
      * sendDate: `{Integer}` * INteiro que representa o período em horas que o email permanecerá agendado. São aceitos valores entre 1 e 72 representando 1 hr e 72 hrs respectivamente.
    * OUT:
      * id: `{uuid}` - Identificador no formato uuid v4 que é retornado ao cliente em caso de agendamento de email. Esse identificador é utilizado na operação de cancelamento de agendamento.
  * `/email/v1/send`
    * IN:
      * id: `{uuid}` - Identificador no formato uuid v4 representando um email que está na lista de agendados. Caso o email não seja localizado, um erro 500 será retornado.
    * OUT:
  * `/email/v1/template`
    * IN:
      * content: `{string (text/html)}` - Conteúdo que será gravado no arquivo de template. A formatação deverá seguir os padrões definidos pelo html5.
    * OUT:
      * id: `{string}` - Identificador uuid/4 que representa o nome do template criado.
  * `/email/v1/template`
    * IN:
      * id: `{string}` - Identificador no formato uuid/4 que representa o template a ser removido.
    * OUT:

* Variáveis de ambiente:
  * HTTP_PORT `{Integer}` - Define a porta ao qual o serviço irá export os métodos http
  * GRPC_URL `{String}` - Url de conexão do GRPC
  * ROOT_CERTS `{String}` - Certificado de CA raiz para validar certificados de cliente
  * PRIVATE_KEY `{String}` - Chave privada do servidor
  * CERT_CHAIN `{String}` - Cadeia de certificados do servidor
  * LOG_ERRORS `{Boolean}` - Se true, os erros devem ser registrados no alive
  * REDIS_URL `{String}` - Url de conexão redis
  * EMAIL_TIMEOUT `{Integer}` - Define a periodicidade para checagem de emails agendados
  * SMTP_PORT `{Integer}` - Define a porta SMTP
  * SMTP_HOST `{String}` - Define o host SMTP
  * SMTP_SECURITY `{Boolean}` - Define se a conexeção SMTP será estabelecida usando mecanismos seguros.
  * SMTP_USER `{String}` - Define o usuário SMTP.
  * SMTP_PASSWORD `{String}` - Define a senha de usuário SMTP.
  * EMAIL_TIMEOUT `{Integer}` - Define o período em ms para checagem da lista de emails agendados.
  * TEMPLATE_PATH `{String}` - Define o path onde os templates serão salvos
  * TDD_EMAIL_FROM `{String}` - Remetente utilizado nos testes
  * TDD_EMAIL_TO `{String}` - Destinatário utilizado nos testes

# TDD
* Comando: `npm run test`
* Para realizar o teste de unidade é obrigatório:
  * Declarar as variáveis de ambiente.
