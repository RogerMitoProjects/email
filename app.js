'use strict';

const fs = require('fs');
const cors = require('cors');
const async = require('async');
const https = require('https');
const morgan = require('morgan');
const express = require('express');
const bodyParser = require('body-parser');
const { logger, constant } = require('lib-commons');
const { grpc, redis, scheduler } = require('./libs');

const app = express();

/**
 * @description Obter as credenciais de conexão HTTPS
 * @param {Function} callback - chamada de retorno `(err, creds) => {};`
 */
const getCredentials = callback => {
  if (process.env.PRIVATE_KEY && process.env.CERT_CHAIN && process.env.ROOT_CERTS) {
    try {
      const creds = {
        key: fs.readFileSync(process.env.PRIVATE_KEY),
        cert: fs.readFileSync(process.env.CERT_CHAIN),
        ca: fs.readFileSync(process.env.ROOT_CERTS)
      };
      callback(null, creds);
    } catch (err) {
      logger(err);
      callback(err);
    }
  } else {
    callback();
  }
};

/**
 * @description Inicializa o microsserviço
 * @param {Function} callback - chamada de retorno `err => {};`
 */
exports.init = callback => {
  async.series(
    [
      callback => grpc.openConnection(callback),
      callback => redis.openConnection(process.env.REDIS_URL, callback),
      callback => scheduler.init(callback),
      callback => {
        const fs = require('fs');
        const path = require('path');
        fs.copyFile(
          path.join(__dirname, 'email_failure.html'),
          path.join(process.env.TEMPLATE_PATH, 'email_failure.html'),
          callback
        );
      }
    ],
    err => {
      if (err) return callback(err);

      if (process.env.NODE_ENV !== 'test') app.use(morgan(constant.morgan));

      app.use(bodyParser.json(), bodyParser.text({ type: 'text/*' }));
      app.use(bodyParser.urlencoded({ extended: false }));
      app.use(cors());

      app.use('/email', require('./routes'));

      getCredentials((err, creds) => {
        if (err) return callback(err);

        if (creds) https.createServer(creds, app).listen(process.env.HTTP_PORT, callback);
        else app.listen(process.env.HTTP_PORT, callback);
      });
    }
  );
};
