'use strict';

const { grpc } = require('lib-commons');
const emailService = require('../services/emailService');

exports.send = (req, res, callback) => {
  const { from, to, cc, mapping, subject, attachments, sendDate, template } = req.body;

  emailService.send({ request: { from, to, cc, mapping, subject, attachments, sendDate, template } }, (err, res) => {
    if (err) return callback(...grpc.parseErr(err));

    callback(200, res);
  });
};

exports.cancel = (req, res, callback) => {
  const { id } = req.body;

  emailService.cancel({ request: { id } }, (err, res) => {
    if (err) return callback(...grpc.parseErr(err));

    callback(200, res);
  });
};
