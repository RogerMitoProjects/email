'use strict';

const { grpc } = require('lib-commons');
const templateService = require('../services/templateService');

exports.create = (req, res, callback) => {
  const content = req.body;

  templateService.create({ request: { content } }, (err, res) => {
    if (err) return callback(...grpc.parseErr(err));

    callback(200, res);
  });
};

exports.delete = (req, res, callback) => {
  const { id } = req.body;

  templateService.delete({ request: { id } }, (err, res) => {
    if (err) return callback(...grpc.parseErr(err));

    callback(200, res);
  });
};
