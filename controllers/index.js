'use strict';

module.exports = {
  sendController: require('./sendController'),
  templateController: require('./templateController')
};
