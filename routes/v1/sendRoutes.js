'use strict';

const router = require('express').Router();
const { handler } = require('lib-commons');
const { sendValidator } = require('../../validator');
const { sendController } = require('../../controllers');

const controller = handler.bind(sendValidator, sendController);

/**
 * @api {post} email/v1/send/ Envia um email
 * @apiName EmailSend
 * @apiGroup Email
 * @apiVersion 1.0.0
 *
 * @apiParam {String} from Email do remetente
 * @apiParam {Array} to Array de strings que contém a lista de destinatários. Não pode ser vazio.
 * @apiParam {Array} cc Array de strings que contém a lista de destinatários em cópia.
 * @apiParam {Object} mapping Objeto contendo as informações para mapeamento de template. A estrutura do objeto deverá ser: {nome_marcacao: conteudo_campo} em que, nome_marcacao deve seguir a mesma nomenclatura definida na marcação do documento HTML.
 * @apiParam {String} subject Assunto do email.
 * @apiParam {Array} attachments Array de objetos contendo os anexos do email. São permitidos até 5 anexos com um tamanho máximo de 10 MB cada. A estrutura do objeto deverá ser, por exemplo: {filename: nome_arquito.extensao, content: string_base64, encoding: base64, contentType: MIME_type Ex.:(application/pdf)}. São permitidos apenas arquivos condificados em base64.
 * @apiParam {Int} [sendDate] Representa o período em horas que o email permanecerá agendado.
 * @apiParam {String} template Identificador do arquivo de template que será utilizado pelo email.
 */
router.post('/', ...controller.send());

/**
 * @api {delete} email/v1/send/ Cancela o agendamento de um email
 * @apiName EmailDelete
 * @apiGroup Email
 * @apiVersion 1.0.0
 *
 * @apiParam {String} id Identificador no formato uuid/v4 que representa o agendamento a ser cancelado.
 */
router.delete('/', ...controller.cancel());

module.exports = router;
