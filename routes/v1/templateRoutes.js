'use strict';

const router = require('express').Router();
const { handler } = require('lib-commons');
const { templateValidator } = require('../../validator');
const { templateController } = require('../../controllers');

const controller = handler.bind(templateValidator, templateController);

/**
 * @api {post} email/v1/template/ Cria um template de email
 * @apiName EmailCreateTemplate
 * @apiGroup Email
 * @apiVersion 1.0.0
 *
 * @apiParam {String} content Conteúdo html do template segundo a estrutura text/html.
 */
router.post('/', ...controller.create());

/**
 * @api {delete} email/v1/template/ Remove um template de email
 * @apiName EmailDeleteTemplate
 * @apiGroup Email
 * @apiVersion 1.0.0
 *
 * @apiParam {String} id Identificador no formato uuid/v4 do template a ser removido.
 */
router.delete('/', ...controller.delete());

module.exports = router;
