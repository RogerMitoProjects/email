'use strict';

const express = require('express');
const router = express.Router();

router.use('/send', require('./sendRoutes'));
router.use('/template', require('./templateRoutes'));

module.exports = router;
