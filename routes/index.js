'use strict';

const express = require('express');

const router = express.Router();

/**
 * @api {all} email/ping Test ping
 * @apiName EmailPing
 * @apiGroup Email
 * @apiVersion 1.0.0
 */
router.all('/ping', (req, res) => res.send({ ping: 'pong' }));

router.use('/v1', require('./v1'));

module.exports = router;
