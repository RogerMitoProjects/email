'use strict';

const { validator } = require('lib-commons');
const { body } = require('express-validator/check');

/**
 * @description Validação dos parâmetros de ...   body('sendDate').isISO8601()
 */
exports.send = validator.route([
  body(['to', 'cc', 'attachments']).isArray(),
  body(['attachments']).custom(attachments => {
    return attachments.length < 4;
  }),
  body('mapping').custom(mapping => {
    return Object.keys(mapping).length > 0;
  }),
  body('template').isUUID(),
  body('from').isEmail(),
  body('sendDate')
    .isInt({ min: 1, max: 72 })
    .optional({ nullable: true })
]);

/**
 * @description Validação dos parâmetros cancel
 */
exports.cancel = validator.route(body('id').isUUID());
