'use strict';

const { validator } = require('lib-commons');
const { body } = require('express-validator/check');

exports.create = [];

exports.delete = validator.route(body('id').isUUID());
