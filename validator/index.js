'use strict';

module.exports = {
  sendValidator: require('./sendValidator'),
  templateValidator: require('./templateValidator')
};
