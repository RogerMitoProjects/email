FROM node:10-alpine

ENV HOME='/usr/src/app'

EXPOSE 3000

WORKDIR $HOME
COPY ./ $HOME

RUN    apk add --no-cache git \
    && npm install --only=prod \
    && npm rebuild --quiet \
    && apk del git

CMD [ "npm", "start" ]
