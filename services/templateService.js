'use strict';

const fs = require('fs');
const path = require('path');
const grpc = require('grpc');
const uuid = require('uuid/v4');
const { logger } = require('../utils');

const regex = /\{\{[a-z0-9]{3,20}\}\}/g;

/**
 * @description Realiza a criação de um template.
 * @param {string} content - Conteúdo que será gravado no arquivo de template. A formatação deverá seguir os padrões definidos
 * pelo html5.
 * @param {string} callback - Respostas HTTP/GRPC. Retorna um identificador uuid/4 que representa o nome do template criado.
 */
exports.create = (call, callback) => {
  const { content } = call.request;
  const match = content.match(regex);

  // Um erro é retornado caso nenhuma marcação válida seja identificada no documento.
  if (match === null) {
    return callback({
      code: grpc.status.INTERNAL,
      message: 'Invalid HTML! No marker found or incorrect markup parameters.'
    });
  }

  const id = uuid();

  // Grava o arquivo de template no diretório definido por process.env.TEMPLATE_PATH
  fs.writeFile(path.join(process.env.TEMPLATE_PATH, `${id}.html`), content, 'utf8', err => {
    if (err) {
      logger.error(err);
      return callback({ code: grpc.status.INTERNAL, message: err });
    }
    callback(null, { id });
  });
};

/**
 * @description Realiza a remoção de um template.
 * @param {string} id - Identificador no formato uuid/4 que representa o template a ser removido.
 * @param {string} callback - Respostas HTTP/GRPC.
 */
exports.delete = (call, callback) => {
  const { id } = call.request;
  const file = path.join(process.env.TEMPLATE_PATH, `${id}.html`);

  if (!fs.existsSync(file)) return callback({ message: 'template not found', code: grpc.status.NOT_FOUND });

  // Remove o arquivo de template no diretório definido por process.env.TEMPLATE_PATH
  fs.unlink(file, err => {
    if (err) return logger.error(err), callback({ message: err, code: grpc.status.INTERNAL });
    else callback(null, { result: true });
  });
};

/**
 * @description Ping
 * @param {string} callback - Callback de retorno `(err, res) => {};`
 */
exports.ping = (call, callback) => callback(null, { ping: 'pong' });
