'use strict';

const fs = require('fs');
const grpc = require('grpc');
const path = require('path');
const uuid = require('uuid/v4');
const redis = require('../libs/redis');
const { logger, constants } = require('../utils');
const nodemailer = require('nodemailer');
const { emailSettings } = require('../utils/constants');

// Realiza o agendamento de um email no servidor REDIS.
function scheduledEmail(email, callback) {
  const id = uuid();
  redis.set(`${constants.namespace}:${id}`, JSON.stringify(email), null, err => {
    callback(err, id);
  });
}

function getBodyFromTemplate(template, mapping, callback) {
  fs.readFile(path.join(process.env.TEMPLATE_PATH, `${template}.html`), (err, content) => {
    if (err) return logger.error(err), callback({ message: err, code: grpc.status.INTERNAL });
    content = content.toString();
    // Preenchimento de template
    const html = Object.keys(mapping).reduce(
      (prevVal, currVal) => prevVal.replace(new RegExp(`{{${currVal}}}`, 'g'), mapping[currVal]),
      content
    );
    if (html === content)
      return callback({ message: 'Was no found any markup to replace', code: grpc.status.INTERNAL });
    return callback(null, html);
  });
}

/**
 * @description Realiza o envio de um e-mail.
 * @param {string} from -Email do remetente
 * @param {array} to - Array de strings que contém a lista de destinatários. Não pode ser vazio.
 * @param {array} cc - Array de strings que contém a lista de destinatários em cópia. Não pode ser vazio.
 * @param {object} mapping - Objeto contendo as informações para mapeamento de template. A estrutura do objeto deverá
 * ser: {nome_marcacao: conteudo_campo} em que, nome_marcacao deve seguir a mesma nomenclatura definida na marcação do
 * documento HTML
 * @param {string} subject - Assunto do email
 * @param {array} attachments - Array de objetos contendo os anexos do email. São permitidos até 5 anexos com um tamanho máximo de 10 MB cada.
 * A estrutura do objeto deverá ser:
 * {filename: nome_arquito.extensao, content: string_base64, encoding: base64, contentType: MIME_type Ex.:(application/pdf)}.
 * São permitidos apenas arquivos condificados em base64.
 * @param {int} sendDate - Inteiro que representa o período em horas que o email permanecerá agendado.
 * São aceitos valores entre 1 e 72 representando 1 hr e 72 hrs respectivamente.
 * @param {string} template - Identificador do arquivo de template que será utilizado pelo email.
 * @param {object} callback - Respostas HTTP/GRPC. Caso seja invocado o método de agendamento de email, um identificador uuid/4 será retornado
 * representando o agendamento realizado.
 */
exports.send = (call, callback) => {
  const { from, to, cc, subject, attachments, sendDate, template } = call.request;
  let { mapping } = call.request;
  const email = { from, to, cc, subject, attachments, sendDate, template, mapping };

  if (typeof mapping === 'string') mapping = JSON.parse(mapping);
  // Validação de tamanho de anexo
  if (attachments.length > 0) {
    if (attachments.find(el => Buffer.byteLength(el.content, 'utf8') / 1000000.0 > 10.0))
      return callback({ message: 'File size too large. You can upload file up to 10 MB.', code: grpc.status.INTERNAL });
  }
  // Caso a data de envio não seja especificada, envia o email imediatamente.
  if (!sendDate) {
    getBodyFromTemplate(template, mapping, (err, res) => {
      if (err) return callback(err);
      // Objeto de email
      email.html = res;
      const settings = emailSettings(email);
      const transporter = nodemailer.createTransport(settings.transporter);

      transporter.sendMail(settings.options, err => {
        if (err) return logger.error(err), callback({ message: err.message, code: grpc.status.INTERNAL });
        else callback(null, { id: '00000000-0000-0000-0000-000000000000' });
      });
    });
  }
  // Envia para a lista de agendados.
  else {
    // Armazena a data e horário que o registro foi salvo
    const now = new Date();
    // now.setHours(now.getMinutes() + email.sendDate);
    process.env.NODE_ENV === 'development'
      ? now.setMinutes(now.getHours() + email.sendDate)
      : now.setHours(now.getMinutes() + email.sendDate);
    email.sendDate = now;

    scheduledEmail(email, (err, res) => {
      if (err) logger.error(err), callback({ message: err.message, code: grpc.status.INTERNAL });
      else callback(null, { id: res });
    });
  }
};

/**
 * @description Realiza o cancelamento de um e-mail na lista de agendados.
 * @param {string} callback - Identificador de e-mail gerado pelo redis.
 */
exports.cancel = (call, callback) => {
  redis.del(`${constants.namespace}:${call.request.id}`, (err, res) => {
    if (err) logger.error(err), callback({ message: err.message, code: grpc.status.INTERNAL });
    else if (res === 0) callback({ message: 'Email not found', code: grpc.status.NOT_FOUND });
    else callback(null, { result: true });
  });
};

/**
 * @description Ping
 * @param {string} callback - Callback de retorno `(err, res) => {};`
 */
exports.ping = (call, callback) => callback(null, { ping: 'pong' });
