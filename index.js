'use strict';

if (process.env.NODE_ENV !== 'production') require('dotenv').config();

const { env } = require('lib-commons');

env.validate(['GRPC_URL', 'SMTP_HOST', 'SMTP_USER', 'SMTP_PASSWORD', 'TEMPLATE_PATH', 'REDIS_URL'], env.check.isString);
env.validate(['HTTP_PORT', 'SMTP_PORT', 'EMAIL_TIMEOUT'], env.check.isNumber);
env.validate(['SMTP_SECURITY'], env.check.isBoolean);

const app = require('./app');
app.init(err => {
  if (err) throw err;

  console.info(`Ready! (${process.env.NODE_ENV})`);
});
